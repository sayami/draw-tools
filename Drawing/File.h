//
//  File.h
//  Drawing
//
//  Created by Viet Trinh on 3/13/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>

@class CDCircle, CDLine, CDRectangle;

@interface File : NSManagedObject

@property (nonatomic, retain) NSString * name;
@property (nonatomic, retain) NSSet *hasLines;
@property (nonatomic, retain) NSSet *hisCircles;
@property (nonatomic, retain) NSSet *hasRectangles;
@end

@interface File (CoreDataGeneratedAccessors)

- (void)addHasLinesObject:(CDLine *)value;
- (void)removeHasLinesObject:(CDLine *)value;
- (void)addHasLines:(NSSet *)values;
- (void)removeHasLines:(NSSet *)values;

- (void)addHisCirclesObject:(CDCircle *)value;
- (void)removeHisCirclesObject:(CDCircle *)value;
- (void)addHisCircles:(NSSet *)values;
- (void)removeHisCircles:(NSSet *)values;

- (void)addHasRectanglesObject:(CDRectangle *)value;
- (void)removeHasRectanglesObject:(CDRectangle *)value;
- (void)addHasRectangles:(NSSet *)values;
- (void)removeHasRectangles:(NSSet *)values;

@end
