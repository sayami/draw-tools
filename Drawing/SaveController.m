//
//  SaveController.m
//  Drawing
//
//  Created by Viet Trinh on 2/28/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "SaveController.h"

@implementation SaveController
@synthesize nameTextField;
@synthesize managedObjectContext;
- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
        saveArrayShape =  [[NSMutableArray new] init];
    }
    return self;
}

- (void)didReceiveMemoryWarning
{
    // Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
    
    // Release any cached data, images, etc that aren't in use.
}

#pragma mark - View lifecycle

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    NSLog(@"Size of  CDArray = %d",[saveArrayShape count]);
}

- (void)viewDidUnload
{
    [self setNameTextField:nil];
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    // Return YES for supported orientations
	return YES;
}

- (IBAction)save_CancelButtonClicked:(id)sender {
    [self.view removeFromSuperview];
    //[self viewDidUnload];
}

- (IBAction)save_SaveButtonClicked:(id)sender {
    [self.view removeFromSuperview];
    //[self viewDidUnload];
}
@end
