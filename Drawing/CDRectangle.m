//
//  CDRectangle.m
//  Drawing
//
//  Created by Viet Trinh on 3/13/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "CDRectangle.h"
#import "File.h"


@implementation CDRectangle

@dynamic sWidth;
@dynamic isSolid;
@dynamic isFill;
@dynamic sRed;
@dynamic sGreen;
@dynamic sBlue;
@dynamic fsRed;
@dynamic fsGreen;
@dynamic fsBlue;
@dynamic topX;
@dynamic topY;
@dynamic bottomX;
@dynamic bottomY;
@dynamic whichFile;

@end
