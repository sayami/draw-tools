//
//  ViewController.h
//  Drawing
//
//  Created by Viet Trinh on 2/8/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Canvas.h"
#import "SaveController.h"
#import "LoadController.h"
@interface ViewController : UIViewController
{
    IBOutlet UIView *view;
    IBOutlet Canvas *canvas;
    
    // Adding other views
    IBOutlet SaveController* saveView;
    IBOutlet LoadController* loadView;
    
    // Adding CoreData
    NSMutableArray * filesArray;
    NSManagedObjectContext * managedObjectContext;
}
- (IBAction)chooseOption:(id)sender;
- (IBAction)undo_Clicked:(id)sender;

- (IBAction)Solid_Or_Dash:(id)sender;
- (IBAction)set_Line_Width:(id)sender;
- (IBAction)set_is_Fill:(id)sender;

- (IBAction)set_Red:(id)sender;
- (IBAction)set_Green:(id)sender;
- (IBAction)set_Blue:(id)sender;

- (IBAction)fill_Red:(id)sender;
- (IBAction)fill_Green:(id)sender;
- (IBAction)fill_Blue:(id)sender;

@property (strong, nonatomic) IBOutlet UIView *StrokeColorView;
@property (strong, nonatomic) IBOutlet UISlider *strokeRedvalue;
@property (strong, nonatomic) IBOutlet UISlider *strokeGreenvalue;
@property (strong, nonatomic) IBOutlet UISlider *strokeBluevalue;

@property (strong, nonatomic) IBOutlet UIView *FillColorView;
@property (strong, nonatomic) IBOutlet UISlider *fillRedValue;
@property (strong, nonatomic) IBOutlet UISlider *fillGreenValue;
@property (strong, nonatomic) IBOutlet UISlider *fillBlueValue;

// Adding new views
- (IBAction)newButtonClicked:(id)sender;
- (IBAction)saveButtonClicked:(id)sender;
- (IBAction)loadButtonClicked:(id)sender;

// Adding CoreData
@property (nonatomic, retain) NSMutableArray * filesArray;
@property (nonatomic, retain) NSManagedObjectContext * managedObjectContext;
@end
