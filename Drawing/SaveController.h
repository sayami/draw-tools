//
//  SaveController.h
//  Drawing
//
//  Created by Viet Trinh on 2/28/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SaveController : UIViewController{
    @public NSMutableArray* saveArrayShape;
    @public NSManagedObjectContext * managedObjectContext;
}

@property (strong, nonatomic) IBOutlet UITextField *nameTextField;
@property (nonatomic, retain) NSManagedObjectContext * managedObjectContext;

- (IBAction)save_CancelButtonClicked:(id)sender;
- (IBAction)save_SaveButtonClicked:(id)sender;

@end
