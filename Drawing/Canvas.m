//
//  Canvas.m
//  Drawing
//
//  Created by Viet Trinh on 2/8/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "Canvas.h"

@implementation Canvas

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
        self.backgroundColor = [UIColor whiteColor];
        //self.clearsContextBeforeDrawing = YES;
        opt = 0;
        twice = 2;
        shape_width = 1.0;
        is_solid = YES;
        is_fill = NO;
        shapeArray = [[NSMutableArray new] init];
        
    }
    return self;
}

-(void)setOpt:(NSInteger)optParm{
    opt = optParm;
}

-(void)set_ShapeWidth:(CGFloat)newWidth{
    shape_width = newWidth;
}

-(void)set_Solid:(BOOL)is_cur_On{
    is_solid = is_cur_On;
    if (is_solid)
        NSLog(@"Solid ON");
    else
        NSLog(@"Solid OFF");
}

-(void)set_Red:(CGFloat)parmR{
    red = parmR;
}

-(void)set_Green:(CGFloat)parmG{
    green = parmG;
}

-(void)set_Blue:(CGFloat)parmB{
    blue = parmB;
}

-(void)set_Fill:(BOOL)is_cur_On{
    is_fill = is_cur_On;
    if (is_fill) {
        NSLog(@"YES! FILL");
    }
}

-(void)set_F_Red:(CGFloat)parmFR{
    f_red = parmFR;
    NSLog(@"fR = %f",f_red);
}

-(void)set_F_Green:(CGFloat)parmFG{
    f_green = parmFG;
     NSLog(@"fG = %f",f_green);
}

-(void)set_F_Blue:(CGFloat)parmFB{
    f_blue = parmFB;
     NSLog(@"fB = %f",f_blue);
}


-(CGFloat)get_ShapeWidth{
    return shape_width;
}

-(BOOL)get_Solid{
    return is_solid;
}

-(CGFloat)get_Red{
    return red;
}

-(CGFloat)get_Green{
    return green;
}

-(CGFloat)get_Blue{
    return blue;
}

-(BOOL)get_Fill{
    return is_fill;
}

-(CGFloat)get_F_Red{
    return f_red;
}

-(CGFloat)get_F_Green{
    return f_green;
}

-(CGFloat)get_F_Blue{
    return f_blue;
}


// location where finger touches down the screen
-(void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event
{
    UITouch *touch = [touches anyObject];
    init_touch = [touch locationInView:self];
    cur_touch = init_touch;
    
    if (opt==1){
        cur_line = [[Line new] init];
        [cur_line initLine];
        cur_line->line_start = init_touch;
    }
    
    else if (opt == 2){   
        cur_circle = [Circle new];
        [cur_circle initCir];
    }
    
    else if (opt == 3){
        cur_rect = [Rectangle new];
        [cur_rect initRect];
    }
    else
    {;}
        
}

// location where finger moves on the screen
-(void)touchesMoved:(NSSet *)touches withEvent:(UIEvent *)event
{
    UITouch *touch = [touches anyObject];   
    cur_touch = [touch locationInView:self];   
        
    [self setNeedsDisplay];
       
}        
    
// location where finger lifts off the screen
-(void)touchesEnded:(NSSet *)touches withEvent:(UIEvent *)event
{
    UITouch *touch = [touches anyObject];
    last_touch = [touch locationInView:self];
    
    NSLog(@"***ltx = %f,lty = %f",last_touch.x,last_touch.y);
    
    if (opt ==1)        // draw line, get the 1st point
        cur_line->line_end = last_touch;
        
    
    if (opt == 3){      //draw rectangle, determine 2nd point
        twice = (twice * (-1));
        NSLog(@"twice = %d",twice);
    }
    
   [self setNeedsDisplay];

}  

-(void)reDraw_Canvas{
    NSUInteger count = [shapeArray count];
    if (count > 0){
        NSEnumerator* enumerator = [shapeArray objectEnumerator];
        id element;
        NSInteger type;
        
        while (element = [enumerator nextObject]) {
            type = [element getType];
            
            if (type == 1){
                Line* drawingLine = (Line*)element;
                CGContextSetLineWidth(context, drawingLine->sWidth);
                CGContextSetRGBStrokeColor(context, drawingLine->sRed, drawingLine->sGreen, drawingLine->sBlue, 1.0);
                
                if (drawingLine->isSolid){ 
                    //CGContextSetLineCap(context, kCGLineCapButt);
                    const CGFloat noDash [] = {};
                    CGContextSetLineDash(context, 0, noDash, 0);
                }
                else {
                    const CGFloat lengths [] = {5,4};
                    CGContextSetLineDash(context, 2, lengths, 2);
                }             
                
                CGContextMoveToPoint(context, drawingLine->line_start.x,drawingLine->line_start.y);
                CGContextAddLineToPoint(context, drawingLine->line_end.x, drawingLine->line_end.y);
                CGContextStrokePath(context); 
            }
            
            else if (type == 2){
                Circle* drawingCir = (Circle*)element;
                CGContextSetRGBStrokeColor(context, drawingCir->sRed, drawingCir->sGreen, drawingCir->sBlue, 1.0);
                CGContextSetLineWidth(context, drawingCir->sWidth);
                
                CGRect _circle = CGRectMake(drawingCir->cir_center.x, drawingCir->cir_center.y, (drawingCir->cir_radius)*2, (drawingCir->cir_radius)*2);
                
                if (drawingCir->isSolid){ 
                    //CGContextSetLineCap(context, kCGLineCapButt);
                    const CGFloat noDash [] = {};
                    CGContextSetLineDash(context, 0, noDash, 0);
                }
                else {
                    const CGFloat lengths [] = {5,4};
                    CGContextSetLineDash(context, 2, lengths, 2);
                }
                
                if ((drawingCir->isFill)){
                    CGContextSetRGBFillColor(context, drawingCir->fsRed, drawingCir->fsGreen, drawingCir->fsBlue, 1.0);
                    CGContextFillEllipseInRect(context, _circle);
                }
                
                CGContextAddEllipseInRect(context, _circle);
                CGContextStrokePath(context); 
            }
            
            else if (type == 3)
            {
                Rectangle* drawingRect = (Rectangle*)element;
                CGContextSetLineWidth(context, drawingRect->sWidth);
                CGContextSetRGBStrokeColor(context, drawingRect->sRed, drawingRect->sGreen, drawingRect->sBlue, 1.0);
                
                CGRect _rectangle = CGRectMake(drawingRect->rect_top_left.x, drawingRect->rect_top_left.y, drawingRect->rect_bottom_right.x - drawingRect->rect_top_left.x, drawingRect->rect_bottom_right.y - drawingRect->rect_top_left.y);
                
                if (drawingRect->isSolid){ 
                    //CGContextSetLineCap(context, kCGLineCapButt);
                    const CGFloat noDash [] = {};
                    CGContextSetLineDash(context, 0, noDash, 0);
                }
                else {
                    const CGFloat lengths [] = {5,4};
                    CGContextSetLineDash(context, 2, lengths, 2);
                }
                
                if ((drawingRect->isFill)){
                    CGContextSetRGBFillColor(context, drawingRect->fsRed, drawingRect->fsGreen, drawingRect->fsBlue, 1.0);
                    CGContextFillRect(context, _rectangle);
                }
                
                CGContextAddRect(context,_rectangle );
                CGContextStrokePath(context);
            }
            else
            {;}
        } //end while
    }// end if

}

-(void)Undo_Drawing{
    opt = 4;
    
    NSLog(@"Undo last draw");
    NSUInteger count = [shapeArray count];
    
        
    NSLog(@"count = %d",count);
    if (count >0){
        NSEnumerator* last = [shapeArray lastObject];
        [shapeArray removeObject:last];
        NSLog(@"count = %d",[shapeArray count]);
       }

    [self setNeedsDisplay];
}

//// Only override drawRect: if you perform custom drawing.
//// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect 
{
    context = UIGraphicsGetCurrentContext();
    
    // Start re-draw all objects
    [self reDraw_Canvas];

    // Drawing code for new object
    // Draw a line
    CGContextSetRGBStrokeColor(context, red, green, blue, 1.0);
    CGContextSetLineWidth(context, shape_width);
    if (opt==1){
       
       if (is_solid){ 
           //CGContextSetLineCap(context, kCGLineCapButt);
           const CGFloat noDash [] = {};
           CGContextSetLineDash(context, 0, noDash, 0);
        }
       else {
            const CGFloat lengths [] = {5,4};
            CGContextSetLineDash(context, 2, lengths, 2);
        }
        
        CGContextMoveToPoint(context, init_touch.x,init_touch.y);
        CGContextAddLineToPoint(context, cur_touch.x, cur_touch.y);
        CGContextStrokePath(context); 
        
        // Add new line to shape array
        if(cur_line){
            cur_line->sRed = red;
            cur_line->sGreen = green;
            cur_line->sBlue = blue;
            cur_line->sWidth = shape_width;
            cur_line->isSolid = is_solid;
            [shapeArray addObject:cur_line];
        }
        
        NSLog(@"IN LINE");
        NSLog(@"x0 = %f,y0 = %f",init_touch.x,init_touch.y);
        NSLog(@"x1 = %f,y1 = %f",cur_touch.x,cur_touch.y);
    }
    
    // Draw a circle
    else if (opt == 2){
        
        // calculatin circle's radius
        float radius= sqrt((pow(cur_touch.x - init_touch.x,2.0)) 
                           + (pow(cur_touch.y - init_touch.y, 2.0)));
       
        CGRect circle = CGRectMake(init_touch.x - radius, init_touch.y - radius, radius*2, radius*2);
        
        if (is_solid){ 
            //CGContextSetLineCap(context, kCGLineCapButt);
            const CGFloat noDash [] = {};
            CGContextSetLineDash(context, 0, noDash, 0);
        }
        else {
            const CGFloat lengths [] = {5,4};
            CGContextSetLineDash(context, 2, lengths, 2);
        }
        
        if(is_fill){
            NSLog(@"Start Fill Circle...");
            CGContextSetRGBFillColor(context, f_red, f_green, f_blue, 1.0);
            CGContextFillEllipseInRect(context, circle);
        }
        
        CGContextAddEllipseInRect(context, circle);
        CGContextStrokePath(context); 
        
        // Add new circle to shape array
        if(cur_circle){
            cur_circle->sRed = red;
            cur_circle->sGreen = green;
            cur_circle->sBlue = blue;
            cur_circle->sWidth = shape_width;
            cur_circle->isFill = is_fill;
            cur_circle->isSolid = is_solid;
            cur_circle->cir_center.x = init_touch.x - radius;
            cur_circle->cir_center.y = init_touch.y - radius;
            cur_circle->cir_radius = radius;
            
            if (is_fill) {
                cur_circle->fsRed = f_red;
                cur_circle->fsGreen = f_green;
                cur_circle->fsBlue = f_blue;
            }
            
            [shapeArray addObject:cur_circle];
        }  
        
        NSLog(@"IN CIRCLE");
        NSLog(@"x0 = %f,y0 = %f",init_touch.x,init_touch.y);
        NSLog(@"x1 = %f,y1 = %f",cur_touch.x,cur_touch.y);
    }
    
    // Draw a rectangle
    else if (opt ==3){
       
        if (twice < 0){     // just get the top_left point
            top_left = last_touch;
            NSLog(@"---tlx = %f,tly = %f",top_left.x,top_left.y);
        }
        else
        {   
            
            CGRect rectangle =  CGRectMake(top_left.x, top_left.y, last_touch.x - top_left.x, last_touch.y - top_left.y);
                        
            if (is_solid){ 
                //CGContextSetLineCap(context, kCGLineCapButt);
                const CGFloat noDash [] = {};
                CGContextSetLineDash(context, 0, noDash, 0);
            }
            else {
                const CGFloat lengths [] = {5,4};
                CGContextSetLineDash(context, 2, lengths, 2);
            }
            
            if(is_fill){
                NSLog(@"Start Fill Rectangle");
                CGContextSetRGBFillColor(context, f_red, f_green, f_blue, 1.0);
                CGContextFillRect(context, rectangle);
            }
            
            
            CGContextAddRect(context,rectangle);
            CGContextStrokePath(context);
            
            if(cur_rect){
                cur_rect->sRed = red;
                cur_rect->sGreen = green;
                cur_rect->sBlue = blue;
                cur_rect->sWidth = shape_width;
                cur_rect->isSolid = is_solid;
                cur_rect->isFill = is_fill;
                cur_rect->rect_top_left = top_left;
                cur_rect->rect_bottom_right = last_touch;
                
                if (is_fill) {
                    cur_rect->fsRed = f_red;
                    cur_rect->fsGreen = f_green;
                    cur_rect->fsBlue = f_blue;
                }
           
                [shapeArray addObject:cur_rect];
            } 
            
            // Add new rectangle to shape array
            NSLog(@"IN RECT");
            NSLog(@"x0 = %f,y0 = %f",top_left.x,top_left.y);
            NSLog(@"x1 = %f,y1 = %f",last_touch.x,last_touch.y);
        }
    }
    else 
    {
        ;   
    }
    
}
@end
