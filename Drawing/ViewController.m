//
//  ViewController.m
//  Drawing
//
//  Created by Viet Trinh on 2/8/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "ViewController.h"

@implementation ViewController
@synthesize StrokeColorView;
@synthesize strokeRedvalue;
@synthesize strokeGreenvalue;
@synthesize strokeBluevalue;
@synthesize FillColorView;
@synthesize fillRedValue;
@synthesize fillGreenValue;
@synthesize fillBlueValue;

@synthesize filesArray;
@synthesize managedObjectContext;

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Release any cached data, images, etc that aren't in use.
}

#pragma mark - View lifecycle

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view, typically from a nib.
    self.view.backgroundColor = [UIColor blackColor];
    self.StrokeColorView.backgroundColor = [UIColor blackColor];
    self.FillColorView.backgroundColor = [UIColor blackColor];
    canvas = [[Canvas new] initWithFrame:CGRectMake(0, 44, 768, 788)];
    [self.view insertSubview:canvas atIndex:0];          
}

- (void)viewDidUnload
{
    canvas = nil;
    view = nil;
    [self setStrokeColorView:nil];
    [self setStrokeRedvalue:nil];
    [self setStrokeGreenvalue:nil];
    [self setStrokeBluevalue:nil];
    [self setFillColorView:nil];
    [self setFillRedValue:nil];
    [self setFillGreenValue:nil];
    [self setFillBlueValue:nil];
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
}

- (void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
}

- (void)viewWillDisappear:(BOOL)animated
{
	[super viewWillDisappear:animated];
}

- (void)viewDidDisappear:(BOOL)animated
{
	[super viewDidDisappear:animated];
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    // Return YES for supported orientations
    return YES;
}

- (IBAction)chooseOption:(id)sender {
    NSInteger opt = [((UISegmentedControl*)sender) selectedSegmentIndex];
    if (opt == 0) {
        NSLog(@"Clear");
        
        // Store current settings of the canvas
        CGFloat tmp_Width = [canvas get_ShapeWidth];
        BOOL    tmp_Solid = [canvas get_Solid];
        BOOL    tmp_Fill  = [canvas get_Fill];
        
        CGFloat tmp_Red   = [canvas get_Red];
        CGFloat tmp_Green = [canvas get_Green];
        CGFloat tmp_Blue  = [canvas get_Blue];
        
        CGFloat tmp_FRed   = [canvas get_F_Red];
        CGFloat tmp_FGreen = [canvas get_F_Green];
        CGFloat tmp_FBlue  = [canvas get_F_Blue];

        
        // remove old canvas
        [canvas removeFromSuperview];
        canvas = nil;
        
        // create new canvas and initialize it with previous settings
        canvas = [[Canvas new] initWithFrame:CGRectMake(0, 44, 768, 788)];
        [self.view insertSubview:canvas atIndex:0];
        
        [canvas set_ShapeWidth:tmp_Width];
        [canvas set_Solid:tmp_Solid];
        [canvas set_Fill:tmp_Fill];
        [canvas set_Red:tmp_Red];
        [canvas set_Green:tmp_Green];
        [canvas set_Blue:tmp_Blue];
        [canvas set_F_Red:tmp_FRed];
        [canvas set_F_Green:tmp_FGreen];
        [canvas set_F_Blue:tmp_FBlue];
        
    }
    else if (opt == 1)
    {
        NSLog(@"draw line");
        [canvas setOpt:1];
    }
    else if(opt == 2)
    {
        NSLog(@"draw circle");
        [canvas setOpt:2];
    }    
    else if (opt == 3)
    {   
        NSLog(@"draw rectangle");
        [canvas setOpt:3];
    }    
    else 
    {
        ;
    }

}


- (IBAction)undo_Clicked:(id)sender {
    NSLog(@"Undo drawing");
    [canvas Undo_Drawing];
}


- (IBAction)Solid_Or_Dash:(id)sender {
    BOOL is_cur_On = [sender isOn];
    [canvas set_Solid:is_cur_On];
}

- (IBAction)set_Line_Width:(id)sender {
    CGFloat cur_width =  ((UISlider*)sender).value;
    [canvas set_ShapeWidth:cur_width];
}

- (IBAction)set_is_Fill:(id)sender {
    [canvas set_Fill:[sender isOn]];
}

- (IBAction)set_Red:(id)sender {
    CGFloat red = ((UISlider*)sender).value;
    
     self.StrokeColorView.backgroundColor = [UIColor colorWithRed:red green:strokeGreenvalue.value blue:strokeBluevalue.value alpha:1.0];
     
    [canvas set_Red:red];
    
    
}

- (IBAction)set_Green:(id)sender {
    CGFloat green = ((UISlider*)sender).value;
    self.StrokeColorView.backgroundColor = [UIColor colorWithRed:strokeRedvalue.value green:green blue:strokeBluevalue.value alpha:1.0];
    [canvas set_Green:green];
}

- (IBAction)set_Blue:(id)sender {
    CGFloat blue = ((UISlider*)sender).value;
    self.StrokeColorView.backgroundColor = [UIColor colorWithRed:strokeRedvalue.value green:strokeGreenvalue.value blue:blue alpha:1.0];
    [canvas set_Blue:blue];
}

- (IBAction)fill_Red:(id)sender {
    self.FillColorView.backgroundColor = [UIColor colorWithRed:((UISlider*)sender).value green:fillGreenValue.value blue:fillBlueValue.value alpha:1.0];
    [canvas set_F_Red:((UISlider*)sender).value];
}

- (IBAction)fill_Green:(id)sender {
    self.FillColorView.backgroundColor = [UIColor colorWithRed:fillRedValue.value green:((UISlider*)sender).value blue:fillBlueValue.value alpha:1.0];
    [canvas set_F_Green:((UISlider*)sender).value];
}

- (IBAction)fill_Blue:(id)sender {
    self.FillColorView.backgroundColor = [UIColor colorWithRed:fillRedValue.value green:fillGreenValue.value blue:((UISlider*)sender).value alpha:1.0];
    [canvas set_F_Blue:((UISlider*)sender).value];
}

- (IBAction)newButtonClicked:(id)sender {
    [canvas removeFromSuperview];
    canvas = [[Canvas new] initWithFrame:CGRectMake(0, 44, 768, 788)];
    [self.view insertSubview:canvas atIndex:0];
}


- (IBAction)saveButtonClicked:(id)sender {
    
    if (saveView == 0) {
        saveView = [[SaveController alloc] initWithNibName:@"SaveController" bundle:nil];
        saveView->saveArrayShape = canvas->shapeArray;
        saveView.managedObjectContext = self.managedObjectContext;
    }
    
    [self.view insertSubview:saveView.view atIndex:1];
    [self.view bringSubviewToFront:saveView.view];
}

- (IBAction)loadButtonClicked:(id)sender {
    if (loadView == 0){
        loadView = [[LoadController alloc] initWithNibName:@"LoadController" bundle:nil];
    }
    [self.view insertSubview:loadView.view atIndex:2];
    [self.view bringSubviewToFront:loadView.view];
    
}
@end
