//
//  File.m
//  Drawing
//
//  Created by Viet Trinh on 3/13/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "File.h"
#import "CDCircle.h"
#import "CDLine.h"
#import "CDRectangle.h"


@implementation File

@dynamic name;
@dynamic hasLines;
@dynamic hisCircles;
@dynamic hasRectangles;

@end
