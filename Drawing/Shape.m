//
//  Shape.m
//  Drawing
//
//  Created by Viet Trinh on 2/10/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "Shape.h"

@implementation Shape
-(NSInteger)getType{
    return type;
}
@end

// Class of Line
@implementation Line

-(void)initLine{
    
    sWidth = 1.0;
    isSolid = YES;
    isFill  = NO;
    type = 1;
    
    line_start.x = 0.0;
    line_start.y = 0.0;
    line_end.x = 0.0;
    line_end.y = 0.0;
    
   
}
@end

// Class of Circle
@implementation Circle

-(void)initCir{
    
    sWidth = 1.0;
    isSolid = YES;
    isFill  = NO;
    type = 2;
    
    cir_center.x = 0.0;
    cir_center.y = 0.0;
    cir_radius = 0.0;
}

@end

// Class of Rectangle
@implementation Rectangle

-(void)initRect{
    
    sWidth = 1.0;
    isSolid = YES;
    isFill  = NO;
    type = 3;
    
    rect_top_left.x = 0.0;
    rect_top_left.y = 0.0;
    rect_bottom_right.x = 0.0;
    rect_bottom_right.y = 0.0;
    
}

@end