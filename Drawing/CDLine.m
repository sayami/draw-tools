//
//  CDLine.m
//  Drawing
//
//  Created by Viet Trinh on 3/13/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "CDLine.h"
#import "File.h"


@implementation CDLine

@dynamic sWidth;
@dynamic isSolid;
@dynamic sRed;
@dynamic sGreen;
@dynamic sBlue;
@dynamic fsRed;
@dynamic fsGreen;
@dynamic fsBlue;
@dynamic startX;
@dynamic starY;
@dynamic endX;
@dynamic endY;
@dynamic whichFile;

@end
