//
//  CDRectangle.h
//  Drawing
//
//  Created by Viet Trinh on 3/13/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>

@class File;

@interface CDRectangle : NSManagedObject

@property (nonatomic, retain) NSNumber * sWidth;
@property (nonatomic, retain) NSNumber * isSolid;
@property (nonatomic, retain) NSNumber * isFill;
@property (nonatomic, retain) NSNumber * sRed;
@property (nonatomic, retain) NSNumber * sGreen;
@property (nonatomic, retain) NSNumber * sBlue;
@property (nonatomic, retain) NSNumber * fsRed;
@property (nonatomic, retain) NSNumber * fsGreen;
@property (nonatomic, retain) NSNumber * fsBlue;
@property (nonatomic, retain) NSNumber * topX;
@property (nonatomic, retain) NSNumber * topY;
@property (nonatomic, retain) NSNumber * bottomX;
@property (nonatomic, retain) NSNumber * bottomY;
@property (nonatomic, retain) File *whichFile;

@end
