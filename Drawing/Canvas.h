//
//  Canvas.h
//  Drawing
//
//  Created by Viet Trinh on 2/8/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Shape.h"

@interface Canvas : UIView
{

    NSInteger opt;      // which one to draw
    NSInteger twice;    // is rect get 2 points
    
    CGContextRef context;
    CGPoint init_touch;
    CGPoint last_touch;
    CGPoint cur_touch;
    CGPoint top_left;   // init point for rect
   
    @public NSMutableArray* shapeArray;
    Line* cur_line;
    Circle* cur_circle;
    Rectangle* cur_rect;
    
    CGFloat shape_width;
    BOOL is_solid;
    BOOL is_fill;
    
    CGFloat red;
    CGFloat green;
    CGFloat blue;
    
    CGFloat f_red;
    CGFloat f_green;
    CGFloat f_blue;
    
    
}

-(void)setOpt:(NSInteger)optParm;
-(void)set_ShapeWidth:(CGFloat)newWidth;
-(void)set_Solid:(BOOL)is_cur_On;
-(void)set_Red:(CGFloat)parmR;
-(void)set_Green:(CGFloat)parmG;
-(void)set_Blue:(CGFloat)parmB;

-(void)set_Fill:(BOOL)is_cur_On;
-(void)set_F_Red:(CGFloat)parmFR;
-(void)set_F_Green:(CGFloat)parmFG;
-(void)set_F_Blue:(CGFloat)parmFB;


-(CGFloat)get_ShapeWidth;
-(BOOL)get_Solid;
-(CGFloat)get_Red;
-(CGFloat)get_Green;
-(CGFloat)get_Blue;

-(BOOL)get_Fill;
-(CGFloat)get_F_Red;
-(CGFloat)get_F_Green;
-(CGFloat)get_F_Blue;

-(void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event;
-(void)touchesMoved:(NSSet *)touches withEvent:(UIEvent *)event;
-(void)touchesEnded:(NSSet *)touches withEvent:(UIEvent *)event;

-(void)reDraw_Canvas;
-(void)Undo_Drawing;
@end



