//
//  Shape.h
//  Drawing
//
//  Created by Viet Trinh on 2/10/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Shape : NSObject
{
@public 
    NSInteger type;
    CGFloat sWidth;
    BOOL isSolid;
    BOOL isFill;
    CGFloat sRed;
    CGFloat sGreen;
    CGFloat sBlue;
    
    CGFloat fsRed;
    CGFloat fsGreen;
    CGFloat fsBlue;
    // 1 is line, 2 is circle, 3 is rectangle
}
- (NSInteger)getType;
@end

// Class of Line
@interface Line : Shape{

    @public CGPoint line_start;
    @public CGPoint line_end;
}
-(void) initLine;
@end

// Class of Circle
@interface Circle :Shape{
@public
    CGPoint cir_center;
    CGFloat cir_radius;
}
-(void) initCir;
@end


//Class of Rectangle
@interface Rectangle :Shape{
@public
    CGPoint rect_top_left;
    CGPoint rect_bottom_right;
}
-(void) initRect;
@end