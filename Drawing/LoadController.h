//
//  LoadController.h
//  Drawing
//
//  Created by Viet Trinh on 3/13/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface LoadController : UITableViewController
@property (strong, nonatomic) IBOutlet UITableView *tableView;
- (IBAction)loadBackButton:(id)sender;

@end
