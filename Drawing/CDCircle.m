//
//  CDCircle.m
//  Drawing
//
//  Created by Viet Trinh on 3/13/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "CDCircle.h"
#import "File.h"


@implementation CDCircle

@dynamic sWidth;
@dynamic isSolid;
@dynamic isFill;
@dynamic sRed;
@dynamic sGreen;
@dynamic sBlue;
@dynamic fsRed;
@dynamic fsGreen;
@dynamic fsBlue;
@dynamic radius;
@dynamic centerX;
@dynamic centerY;
@dynamic whichFile;

@end
